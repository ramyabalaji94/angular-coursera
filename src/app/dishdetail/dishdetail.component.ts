import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Comment} from '../shared/comment';

import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Dish } from '../shared/dish';
import { DishService } from '../services/dish.service';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss']
})

export class DishdetailComponent implements OnInit {
  commentForm: FormGroup;
  comment: Comment;
  dish : Dish;
  dishIds: number[];
  prev: number;
  next: number;
  formErrors = {
  'author': '',
  'rating': '',
  'comment': ''
  };
  validationMessages = {
  'author': {
  'required': 'Name is required.',
  'minlength': 'Name must be at least 2 characters long.',
  'maxlength': 'Name cannot be more than 25 characters long.'
  },
  'comment': {
  'required': 'Comment is required.'
  },
  'rating': {
  'required': 'Tel. number is required.',
  'pattern': 'Tel. number must contain only numbers.'
  }
  };
  constructor(private dishservice: DishService,
   private route: ActivatedRoute,
   private location: Location, private fb: FormBuilder) { this.createForm();}

   ngOnInit() {
       this.dishservice.getDishIds().subscribe(dishIds => this.dishIds = dishIds);
       this.route.params
         .switchMap((params: Params) => this.dishservice.getDish(+params['id']))
         .subscribe(dish => { this.dish = dish; this.setPrevNext(dish.id); });
     }
     setPrevNext(dishId: number) {
    let index = this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[(this.dishIds.length + index - 1)%this.dishIds
      .length];
    this.next = this.dishIds[(this.dishIds.length + index + 1)%this.dishIds
      .length];
  }
  createForm() {
    this.commentForm = this.fb.group({
  author: ['', [Validators.required, Validators.minLength(2)] ],
  rating:5,
  comment: ['', [Validators.required] ]
  });
  this.commentForm.valueChanges
.subscribe(data => this.onValueChanged(data));
this.onValueChanged(); // (re)set validation messages now
}

  onValueChanged(data?: any) {
if (!this.commentForm) { return; }
const form = this.commentForm;
for (const field in this.formErrors) {
// clear previous error message (if any)
this.formErrors[field] = '';
const control = form.get(field);
if (control && control.dirty && !control.valid) {
const messages = this.validationMessages[field];
for (const key in control.errors) {
this.formErrors[field] += messages[key] + ' ';
}
}
}
}
onSubmit() {
this.comment = this.commentForm.value;
console.log(this.comment);
var d = new Date();
var n = d.toISOString();
this.comment.date = n;
this.dish.comments.push(this.comment);
this.commentForm.reset({
author: '',
rating: 5,
comment: '',
date:''
});
}

 goBack(): void {
   this.location.back();
 }

}
